package com.example.recipeapp.Adapter;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.recipeapp.Models.FavouriteRecipeModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.UserViewModel;
import com.example.recipeapp.Views.Fragments.ActiveUserFragments.PreviewRecipeFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FavouriteRecipeAdapter extends RecyclerView.Adapter<FavouriteRecipeAdapter.ViewHolder> {
    private static final String TAG = "FavouriteRecipeAdapter";


    private Context mContext;
    private List<RecipeModel> favouriteRecipeModels;
    private UserViewModel userViewModel;

    public FavouriteRecipeAdapter(Context mContext, List<RecipeModel> favouriteRecipeModels) {

        this.mContext = mContext;
        this.favouriteRecipeModels = favouriteRecipeModels;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recipe_item_layout, viewGroup, false);

        userViewModel = ViewModelProviders.of((FragmentActivity) mContext).get(UserViewModel.class);

        return new FavouriteRecipeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        //checkIfUserSetRecipeToFavourites(position, viewHolder);

        Log.d(TAG, "OBJECT: " + favouriteRecipeModels);

        final RecipeModel recepies = favouriteRecipeModels.get(position);


        viewHolder.recipeTitleTextView.setText(recepies.getTitle());
        viewHolder.shortDescriptionTextView.setText(recepies.getShortDescription());
        viewHolder.dishTypeTextView.setText(recepies.getMealType());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("recipeId", recepies.getId());

                AppCompatActivity activity = (AppCompatActivity) mContext;
                PreviewRecipeFragment myFragment = new PreviewRecipeFragment();
                myFragment.setArguments(bundle);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, myFragment).addToBackStack(null).commit();
            }
        });



    }

//    public void checkIfUserSetRecipeToFavourites(int position, final ViewHolder viewHolder){
//
//        final RecipeModel recepies = favouriteRecipeModels.get(position);
//        userViewModel.getUserFavRecipeIds(new MyFirebaseCallBack<List<String>>() {
//            @Override
//            public void onSuccessCallback(List<String> object) {
//
//                if (object != null){
//
//                    for (int i = 0; i < object.size(); i++){
//                        if (recepies.getId().contains(object.get(i))){
//                            viewHolder.toggleButtonAddToFavourites.setEnabled(false);
//                        }
//                    }
//                }
//
//            }
//
//            @Override
//            public void onFailureCallback(String message) {
//
//            }
//        });
//    }

    @Override
    public int getItemCount() {
        return favouriteRecipeModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView recipeTitleTextView;
        public TextView shortDescriptionTextView;
        public TextView dishTypeTextView;
        public TextView dishRatingTextView;
        public Button buttonAddToFavourites;
        public ToggleButton toggleButtonAddToFavourites;

        public ViewHolder(@NonNull View itemView){
            super(itemView);

            recipeTitleTextView = itemView.findViewById(R.id.recipeTitleTextView);
            shortDescriptionTextView = itemView.findViewById(R.id.shortDescriptionTextView);
            dishTypeTextView = itemView.findViewById(R.id.dishTypeTextView);
            dishRatingTextView = itemView.findViewById(R.id.dishRatingTextView);
            buttonAddToFavourites = itemView.findViewById(R.id.toggleButtonAddToFavourites);
            toggleButtonAddToFavourites = itemView.findViewById(R.id.toggleButtonAddToFavourites);
        }

    }
}
