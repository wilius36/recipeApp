package com.example.recipeapp.Adapter;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.recipeapp.Models.FavouriteRecipeModel;
import com.example.recipeapp.Models.IngredientsModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.RecipeViewModel;
import com.example.recipeapp.ViewModels.UserViewModel;
import com.example.recipeapp.Views.Fragments.ActiveUserFragments.PreviewRecipeFragment;

import java.util.List;

public class RecepieAdapter extends RecyclerView.Adapter<RecepieAdapter.ViewHolder> {

    private static final String TAG = "RecepieAdapter";

    private Context mContext;
    private List<RecipeModel> mRecepies;
    private List<FavouriteRecipeModel> nRecepies;

    private UserViewModel userViewModel;
    private RecipeViewModel recipeViewModel;



    public RecepieAdapter(Context mContext, List<RecipeModel> mRecepies) {
        this.mRecepies = mRecepies;
        this.mContext = mContext;
    }

    public RecepieAdapter (List<FavouriteRecipeModel> nRecepies){
        this.nRecepies = nRecepies;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.recipe_item_layout, viewGroup, false);

        userViewModel = ViewModelProviders.of((FragmentActivity) mContext).get(UserViewModel.class);
        recipeViewModel = ViewModelProviders.of((FragmentActivity) mContext).get(RecipeViewModel.class);


        return new RecepieAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {


        if (mRecepies != null){
            checkIfUserSetRecipeToFavourites(position, viewHolder);
            final RecipeModel recepies = mRecepies.get(position);

            Log.d(TAG, recepies.getMealType());
            viewHolder.recipeTitleTextView.setText(recepies.getTitle());
            viewHolder.shortDescriptionTextView.setText(recepies.getShortDescription());
            viewHolder.dishTypeTextView.setText(recepies.getMealType());

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("recipeId", recepies.getId());

                    AppCompatActivity activity = (AppCompatActivity) mContext;
                    PreviewRecipeFragment myFragment = new PreviewRecipeFragment();
                    myFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, myFragment).addToBackStack(null).commit();
                }
            });



            viewHolder.toggleButtonAddToFavourites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {


                    FavouriteRecipeModel favouriteRecipeModel = new FavouriteRecipeModel();
                    long postTimeInMillis = System.currentTimeMillis();

                    String recipeId = recepies.getId();
                    String creatorId = recepies.getCreatorId();
                    String mealType = recepies.getMealType();
                    String title = recepies.getTitle();
                    String shortDescription = recepies.getShortDescription();
                    int sumOfRating = recepies.getSumOfRating();
                    int totalPeopleRatedCount = recepies.getTotalPeopleRatedCount();
                    String factureDescription = recepies.getFactureDescription();
                    String factureDiffuclty= recepies.getFactureDificulty();
                    long postDate = recepies.getPostDate();
                    List<String> usersWhoRatedRecipeId = recepies.getUsersWhoRatedRecipeId();
                    List<IngredientsModel> ingredients = recepies.getIngredients();



                    favouriteRecipeModel.setDateWhenSetFavourite(postTimeInMillis);
                    favouriteRecipeModel.setRecipeId(recipeId);
                    favouriteRecipeModel.setCreatorId(creatorId);
                    favouriteRecipeModel.setRecipeMealType(mealType);
                    favouriteRecipeModel.setTitle(title);
                    favouriteRecipeModel.setShortDescription(shortDescription);
                    favouriteRecipeModel.setSumOfRating(sumOfRating);
                    favouriteRecipeModel.setTotalPeopleRatedCount(totalPeopleRatedCount);
                    favouriteRecipeModel.setFactureDescription(factureDescription);
                    favouriteRecipeModel.setFactureDificulty(factureDiffuclty);
                    favouriteRecipeModel.setPostDate(postDate);
                    favouriteRecipeModel.setUsersWhoRatedRecipeId(usersWhoRatedRecipeId);
                    favouriteRecipeModel.setIngredients(ingredients);

                    userViewModel.saveRecipeToUserFavourites(favouriteRecipeModel);



                }
            });
        }else {

            final FavouriteRecipeModel favouriteRecipeModel= nRecepies.get(position);

            viewHolder.recipeTitleTextView.setText(favouriteRecipeModel.getTitle());
            viewHolder.shortDescriptionTextView.setText(favouriteRecipeModel.getShortDescription());
            viewHolder.dishTypeTextView.setText(favouriteRecipeModel.getRecipeMealType());
        }


    }

    public void checkIfUserSetRecipeToFavourites(int position, final ViewHolder viewHolder){

        final RecipeModel recepies = mRecepies.get(position);
        userViewModel.getUserFavRecipeIds(new MyFirebaseCallBack<List<String>>() {
            @Override
            public void onSuccessCallback(List<String> object) {

                if (object != null){
                    for (int i = 0; i < object.size(); i++){
                        if (recepies.getId().contains(object.get(i))){
                            viewHolder.toggleButtonAddToFavourites.setEnabled(false);
                        }
                    }
                }



            }

            @Override
            public void onFailureCallback(String message) {

            }
        });
    }



    @Override
    public int getItemCount() {
        if (mRecepies == null){
            return nRecepies.size();
        }else{
            return mRecepies.size();
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView recipeTitleTextView;
        public TextView shortDescriptionTextView;
        public TextView dishTypeTextView;
        public TextView dishRatingTextView;
        public ToggleButton toggleButtonAddToFavourites;




        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            recipeTitleTextView = itemView.findViewById(R.id.recipeTitleTextView);
            shortDescriptionTextView = itemView.findViewById(R.id.shortDescriptionTextView);
            dishTypeTextView = itemView.findViewById(R.id.dishTypeTextView);
            dishRatingTextView = itemView.findViewById(R.id.dishRatingTextView);
            toggleButtonAddToFavourites = itemView.findViewById(R.id.toggleButtonAddToFavourites);

        }
    }
}
