package com.example.recipeapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.recipeapp.Models.FavouriteRecipeModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ViewHolder>{

    private static final String TAG = "RecyclerAdapter";

    private Context mContext;
    private List<List<RecipeModel>> mRecepiesList;

    private List<List<FavouriteRecipeModel>> nRecipesList;

    public RecyclerViewDataAdapter(List<List<RecipeModel>> mRecepiesList, Context mContext) {
        this.mContext = mContext;
        this.mRecepiesList = mRecepiesList;

    }

    public RecyclerViewDataAdapter(List<List<FavouriteRecipeModel>> nRecipesList){

        this.nRecipesList = nRecipesList;

    }

    @NonNull
    @Override
    public RecyclerViewDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.main_recipe_display, viewGroup, false);


        return new RecyclerViewDataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewDataAdapter.ViewHolder itemRowHolder, int position) {


        if (nRecipesList != null){
            Log.d(TAG, "nRecipesList size " + nRecipesList.size());
        }
        //Log.d(TAG, mRecepiesList.get(0) + "");

        if (mRecepiesList != null){
            for (int i = 0; i < mRecepiesList.get(position).size(); i++) {

                Log.d(TAG, "mRecipesList size " + mRecepiesList.size());
                itemRowHolder.mainDishTypeTextView.setText(mRecepiesList.get(position).get(i).getMealType());

                RecepieAdapter itemListDataAdapter = new RecepieAdapter(mContext, mRecepiesList.get(position));
                itemRowHolder.mainDishTypeRecyclerView.setHasFixedSize(true);
                itemRowHolder.mainDishTypeRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                itemRowHolder.mainDishTypeRecyclerView.setAdapter(itemListDataAdapter);
                itemRowHolder.mainDishTypeRecyclerView.setNestedScrollingEnabled(false);
            }
        }
        if (mRecepiesList == null){
            for (int i = 0; i < nRecipesList.get(position).size(); i++){

                Log.d(TAG, "nRecipesList size " + nRecipesList.size());

                RecepieAdapter itemListDataAdapter = new RecepieAdapter(nRecipesList.get(position));
                itemRowHolder.mainDishTypeRecyclerView.setHasFixedSize(true);
                itemRowHolder.mainDishTypeRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                itemRowHolder.mainDishTypeRecyclerView.setAdapter(itemListDataAdapter);
                itemRowHolder.mainDishTypeRecyclerView.setNestedScrollingEnabled(false);
            }
        }


    }

    @Override
    public int getItemCount() {
        if (mRecepiesList == null){
            return nRecipesList.size();
        }else {
            return mRecepiesList.size();
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mainDishTypeTextView;
        public RecyclerView mainDishTypeRecyclerView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mainDishTypeTextView = itemView.findViewById(R.id.mainDishTypeTextView);
            mainDishTypeRecyclerView = itemView.findViewById(R.id.mainDishTypeRecyclerView);
        }
    }
}
