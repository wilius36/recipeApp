package com.example.recipeapp.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Views.Fragments.ActiveUserFragments.PreviewRecipeFragment;

import java.util.List;

public class UserRecipesAdapter extends RecyclerView.Adapter<UserRecipesAdapter.ViewHolder> {

    private static final String TAG = "RecepieAdapter";

    private Context mContext;
    private List<RecipeModel> mRecepies;


    public UserRecipesAdapter(Context mContext, List<RecipeModel> mRecepies) {
        this.mRecepies = mRecepies;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public UserRecipesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recipe_item_layout, viewGroup, false);
        return new UserRecipesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserRecipesAdapter.ViewHolder viewHolder, int i) {

        final RecipeModel recepies = mRecepies.get(i);

        Log.d(TAG, recepies.getMealType());
        viewHolder.recipeTitleTextView.setText(recepies.getTitle());
        viewHolder.shortDescriptionTextView.setText(recepies.getShortDescription());
        viewHolder.dishTypeTextView.setText(recepies.getMealType());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("recipeId", recepies.getId());

                AppCompatActivity activity = (AppCompatActivity) mContext;
                PreviewRecipeFragment myFragment = new PreviewRecipeFragment();
                myFragment.setArguments(bundle);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, myFragment).addToBackStack(null).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mRecepies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView recipeTitleTextView;
        public TextView shortDescriptionTextView;
        public TextView dishTypeTextView;
        public TextView dishRatingTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            recipeTitleTextView = itemView.findViewById(R.id.recipeTitleTextView);
            shortDescriptionTextView = itemView.findViewById(R.id.shortDescriptionTextView);
            dishTypeTextView = itemView.findViewById(R.id.dishTypeTextView);
            dishRatingTextView = itemView.findViewById(R.id.dishRatingTextView);
        }
    }
}
