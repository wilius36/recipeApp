package com.example.recipeapp.Models;

import java.util.List;

public class FavouriteRecipeModel {




    private RecipeModel recipeModel;


    ////////
    private String recipeId;
    private String creatorId;
    private String recipeMealType;
    private String title;
    private String shortDescription;
    private int sumOfRating;
    private int totalPeopleRatedCount;
    private String factureDescription;
    private String factureDificulty;
    private long postDate;
    private long dateWhenSetFavourite;

    private List<String> usersWhoRatedRecipeId;
    private List<IngredientsModel> ingredients;

    public FavouriteRecipeModel(){

    }

    public FavouriteRecipeModel(String recipeId, String creatorId, String recipeMealType,
                                String title, String shortDescription,
                                int sumOfRating, int totalPeopleRatedCount,
                                String factureDescription, String factureDificulty,
                                long postDate, long dateWhenSetFavourite,
                                List<String> usersWhoRatedRecipeId, List<IngredientsModel> ingredients) {
        this.recipeId = recipeId;
        this.creatorId = creatorId;
        this.recipeMealType = recipeMealType;
        this.title = title;
        this.shortDescription = shortDescription;
        this.sumOfRating = sumOfRating;
        this.totalPeopleRatedCount = totalPeopleRatedCount;
        this.factureDescription = factureDescription;
        this.factureDificulty = factureDificulty;
        this.postDate = postDate;
        this.dateWhenSetFavourite = dateWhenSetFavourite;
        this.usersWhoRatedRecipeId = usersWhoRatedRecipeId;
        this.ingredients = ingredients;

    }

    public RecipeModel getRecipeModel() {
        return recipeModel;
    }

    public void setRecipeModel(RecipeModel recipeModel) {
        this.recipeModel = recipeModel;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getRecipeMealType() {
        return recipeMealType;
    }

    public void setRecipeMealType(String recipeMealType) {
        this.recipeMealType = recipeMealType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getSumOfRating() {
        return sumOfRating;
    }

    public void setSumOfRating(int sumOfRating) {
        this.sumOfRating = sumOfRating;
    }

    public int getTotalPeopleRatedCount() {
        return totalPeopleRatedCount;
    }

    public void setTotalPeopleRatedCount(int totalPeopleRatedCount) {
        this.totalPeopleRatedCount = totalPeopleRatedCount;
    }

    public String getFactureDescription() {
        return factureDescription;
    }

    public void setFactureDescription(String factureDescription) {
        this.factureDescription = factureDescription;
    }

    public String getFactureDificulty() {
        return factureDificulty;
    }

    public void setFactureDificulty(String factureDificulty) {
        this.factureDificulty = factureDificulty;
    }

    public long getPostDate() {
        return postDate;
    }

    public void setPostDate(long postDate) {
        this.postDate = postDate;
    }

    public long getDateWhenSetFavourite() {
        return dateWhenSetFavourite;
    }

    public void setDateWhenSetFavourite(long dateWhenSetFavourite) {
        this.dateWhenSetFavourite = dateWhenSetFavourite;
    }


    public List<String> getUsersWhoRatedRecipeId() {
        return usersWhoRatedRecipeId;
    }

    public void setUsersWhoRatedRecipeId(List<String> usersWhoRatedRecipeId) {
        this.usersWhoRatedRecipeId = usersWhoRatedRecipeId;
    }

    public List<IngredientsModel> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientsModel> ingredients) {
        this.ingredients = ingredients;
    }

}
