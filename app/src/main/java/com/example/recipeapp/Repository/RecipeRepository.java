package com.example.recipeapp.Repository;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.recipeapp.Models.FavouriteRecipeModel;
import com.example.recipeapp.Models.IngredientsModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.Models.UserModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

public class RecipeRepository {

    private static final String TAG = "RecipeRepository";

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference recipesCollectionReference;
    private CollectionReference userCollectionReference;
    int count = 0;

    public RecipeRepository() {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        recipesCollectionReference = firebaseFirestore.collection("recipes");
        userCollectionReference = firebaseFirestore.collection("users");
    }

    //Recepto ivertinimas
    public void rateRecipe (final RecipeModel recipeModel, final MyFirebaseCallBack<Boolean> myFirebaseCallBack){

        recipesCollectionReference.document(recipeModel.getId()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        RecipeModel newRecipeModel = document.toObject(RecipeModel.class);

                        int sumOfRating = newRecipeModel.getSumOfRating();
                        int totalPeopleRatedCount = newRecipeModel.getTotalPeopleRatedCount();

                        int userRating = recipeModel.getSumOfRating();
                        sumOfRating += userRating;
                        totalPeopleRatedCount++;

                        Map<String, Object> recipe = new HashMap<>();
                        recipe.put("sumOfRating", sumOfRating);
                        recipe.put("totalPeopleRatedCount", totalPeopleRatedCount);
                        recipe.put("usersWhoRatedRecipeId", FieldValue.arrayUnion(currentUser.getUid()));

                        recipesCollectionReference.document(recipeModel.getId()).update(recipe).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                myFirebaseCallBack.onSuccessCallback(true);
                            }
                        });
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myFirebaseCallBack.onFailureCallback(e.getMessage());
            }
        });

    }

    //Add recipe to database
    public void addRecipe (RecipeModel recipeModel, final MyFirebaseCallBack<Boolean> myFirebaseCallBack){

        final String recipeId = recipesCollectionReference.document().getId();

        Map<String, Object> recipe = new HashMap<>();
        recipe.put("id", recipeId);
        recipe.put("recipeCreatorId", currentUser.getUid());
        recipe.put("mealType", recipeModel.getMealType());
        recipe.put("title", recipeModel.getTitle());
        recipe.put("shortDescription", recipeModel.getShortDescription());
        recipe.put("factureDescription", recipeModel.getFactureDescription());
        recipe.put("factureDificulty", recipeModel.getFactureDificulty());
        recipe.put("postDate", recipeModel.getPostDate());
        recipe.put("sumOfRating", 0);
        recipe.put("totalPeopleRatedCount", 0);

        recipe.put("ingredients", recipeModel.getIngredients());

        recipesCollectionReference.document(recipeId)
                .set(recipe)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        myFirebaseCallBack.onSuccessCallback(true);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        myFirebaseCallBack.onFailureCallback(e.getMessage());
                    }
                });

    }

    //Gaunami vartotojo sukurti receptai
    public void getUserRecipes (final MyFirebaseCallBack<RecipeModel> myFirebaseCallBack){
        recipesCollectionReference.whereEqualTo("recipeCreatorId", currentUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                myFirebaseCallBack.onSuccessCallback(document.toObject(RecipeModel.class));
                            }
                        } else {
                            myFirebaseCallBack.onFailureCallback(task.getException().getMessage());
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    //Recepto atvaizdavimas pagal id
    public void previewRecipe (String recipeId, final MyFirebaseCallBack<RecipeModel> myFirebaseCallBack){
        recipesCollectionReference.document(recipeId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        myFirebaseCallBack.onSuccessCallback(document.toObject(RecipeModel.class));
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myFirebaseCallBack.onFailureCallback(e.getMessage());
            }
        });
    }

    //Gaunami receptai pagal tipa
    public void getRecipesByType (List<String> mDishType, final MyFirebaseCallBack<List<RecipeModel>> myFirebaseCallBack) {

        for (final String type : mDishType) {
            recipesCollectionReference.whereEqualTo("mealType", type).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        List<RecipeModel> mRecepies = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            mRecepies.add(document.toObject(RecipeModel.class));
                           // Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        }
                        myFirebaseCallBack.onSuccessCallback(mRecepies);
                    }
                }
            });
        }
    }




}
