package com.example.recipeapp.Utils;

public interface MyFirebaseCallBackTwoObjects<T,R> {


    void onSuccessCallback(T object1, R object2);

    void onFailureCallback(String message);
}
