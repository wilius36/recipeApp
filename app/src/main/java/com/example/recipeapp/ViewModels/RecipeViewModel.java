package com.example.recipeapp.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.recipeapp.Models.FavouriteRecipeModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.Repository.RecipeRepository;
import com.example.recipeapp.Utils.MyFirebaseCallBack;

import java.util.List;

public class RecipeViewModel extends AndroidViewModel {

    private RecipeModel recipeModel;
    private RecipeRepository recipeRepository;
    private MutableLiveData<RecipeModel> recipeModelMutableLiveData;

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        recipeModel = new RecipeModel();
        recipeRepository = new RecipeRepository();
    }

    //Recipe model metodas skirtas gauti recepto duomenim
    public RecipeModel getRecipeData() {
        return recipeModel;
    }

    //Recepto uzsaugojimas
    public void saveRecipe(MyFirebaseCallBack<Boolean> myFirebaseCallBack) {
        recipeRepository.addRecipe(recipeModel, myFirebaseCallBack);
    }

    //Gaunami vartotojo receptai
    public void getUserRecipes (MyFirebaseCallBack<RecipeModel> myFirebaseCallBack){
        recipeRepository.getUserRecipes(myFirebaseCallBack);
    }

    //Atvaizduojamas receptas
    public void previewRecipe (String recipeId ,MyFirebaseCallBack<RecipeModel> myFirebaseCallBack){
        recipeRepository.previewRecipe(recipeId, myFirebaseCallBack);
    }

    //Recepto ivertinimas
    public void rateRecipe (RecipeModel recipeModel, MyFirebaseCallBack<Boolean> myFirebaseCallBack){
        recipeRepository.rateRecipe(recipeModel, myFirebaseCallBack);
    }

    //Gaunami visi receptai
    public void getRecipesByType (List<String> mDishType, MyFirebaseCallBack<List<RecipeModel>> myFirebaseCallBack) {
        recipeRepository.getRecipesByType(mDishType, myFirebaseCallBack);
    }





}
