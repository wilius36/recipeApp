package com.example.recipeapp.Views.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.UserViewModel;
import com.example.recipeapp.Views.Fragments.ActiveUserFragments.FavoriteFragment;
import com.example.recipeapp.Views.Fragments.ActiveUserFragments.MainFragment;
import com.example.recipeapp.Views.Fragments.ActiveUserFragments.ProfileFragment;
import com.example.recipeapp.Views.Fragments.RecipeAddFragments.FirstAddRecipeFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public FrameLayout mMainFrame;
    public BottomNavigationView mMainNav;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainFrame = findViewById(R.id.main_frame);
        mMainNav = findViewById(R.id.main_nav);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        //Nustatomas pagrindinis fragmentas
        setFragment(new MainFragment());

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                //Cia galima nurodyti ka atliks kiekvienas fragmentas
                switch (item.getItemId()) {
                    case R.id.nav_main:
                        //Su situ kodu galima pakeisti spalva fono apatines juostos kai tas elementas pasirenkamas
                        //mMainNav.setItemBackgroundResource(R.color.colorAccent);
                        setFragment(new MainFragment());
                        return true;

                    case R.id.nav_favorite:
                        setFragment(new FavoriteFragment());
                        return true;

                    case R.id.nav_add:
                        setFragment(new FirstAddRecipeFragment());
                        return true;

                    case R.id.nav_person:
                        setFragment(new ProfileFragment());
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        userViewModel.checkUserType(new MyFirebaseCallBack<String>() {
            @Override
            public void onSuccessCallback(String object) {

                if (object.equals("user")){
                    mMainNav.getMenu().removeItem(R.id.nav_add);
                }
            }

            @Override
            public void onFailureCallback(String message) {

            }
        });
    }

    //Fragmentu keitimo metodas
    public void setFragment (Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    //Paslepia klaviatura
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
