package com.example.recipeapp.Views.Fragments.ActiveUserFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.recipeapp.Adapter.RecyclerViewDataAdapter;
import com.example.recipeapp.Models.FavouriteRecipeModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.Utils.MyFirebaseCallBackTwoObjects;
import com.example.recipeapp.ViewModels.RecipeViewModel;
import com.example.recipeapp.ViewModels.UserViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class FavoriteFragment extends Fragment {

    private static final String TAG = "FavoriteFragment";

    private View view;

    private UserViewModel userViewModel;
    private RecipeViewModel recipeViewModel;

    private RecyclerView recyclerView;

    private List<List<FavouriteRecipeModel>> recipesSortedByTypeAndDateList;

    private List<String> dishTypesList;

    int count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favorite, container, false);

        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
        recipeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);

        viewsInitialization();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        initializeRecyclerView();

        return view;
    }


    private void viewsInitialization() {


        dishTypesList = new ArrayList<>();
        recipesSortedByTypeAndDateList = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recyclerViewFavouritesRecipes);

    }

    private void initializeRecyclerView(){

        String[] stringArray = getResources().getStringArray(R.array.dish_type_array);
        Collections.addAll(dishTypesList, stringArray);

        final List<String> sortedDishTypeList;
//        sortedDishTypeList = favMealType;
        userViewModel.getUserFavoriteRecipes(new MyFirebaseCallBack <List<FavouriteRecipeModel>>() {
            @Override
            public void onSuccessCallback(List<FavouriteRecipeModel> object) {
                count = 0;
                final List<String> favMealType = new ArrayList<>();
                for (int i = 0; i < object.size(); i++){

                  favMealType.add(object.get(i).getRecipeMealType());
                  //Log.d(TAG, "favMealTypes " + favMealType);
                  count++;

                  if (count == object.size()){
                      Log.d(TAG, "favMealTypes " + favMealType);
                      HashSet<String> hashSet = new HashSet<String>(favMealType);
                      favMealType.clear();
                      favMealType.addAll(hashSet);
                      //Log.d(TAG, "favMealTypes " + hashSet);
                      count = 0;

                      userViewModel.getSortedUserFavRecipes(favMealType, new MyFirebaseCallBack<List<FavouriteRecipeModel>>() {
                          @Override
                          public void onSuccessCallback(List<FavouriteRecipeModel> object) {

                              recipesSortedByTypeAndDateList.add(object);
                              count++;
                              Log.d(TAG, "RECIPES TO LIST LIST " + recipesSortedByTypeAndDateList.size());

                              if (count == favMealType.size()){

                                  RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(recipesSortedByTypeAndDateList);
                                  recyclerView.setAdapter(adapter);

                              }

                          }

                          @Override
                          public void onFailureCallback(String message) {

                          }
                      });

                  }

              }

            }

            @Override
            public void onFailureCallback(String message) {

            }
        });


    }

}