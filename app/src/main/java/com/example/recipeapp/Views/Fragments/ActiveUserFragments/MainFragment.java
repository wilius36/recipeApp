package com.example.recipeapp.Views.Fragments.ActiveUserFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.recipeapp.Adapter.RecyclerViewDataAdapter;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.RecipeViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainFragment extends Fragment {

    private static final String TAG = "MainFragment";

    private View view;
    private RecyclerView mainRecyclerView;
    private List<String> mDishType;
    private List<List<RecipeModel>> mRecepiesList;
    private RecipeViewModel recepeViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main, container, false);

        recepeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);

        mDishType = new ArrayList<>();
        mRecepiesList = new ArrayList<>();

        mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
        mainRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        String[] stringArray = getResources().getStringArray(R.array.dish_type_array);
        Collections.addAll(mDishType, stringArray);
        mDishType.remove("Pasirinkite patiekalo tipą");

        recepeViewModel.getRecipesByType(mDishType, new MyFirebaseCallBack<List<RecipeModel>>() {
            @Override
            public void onSuccessCallback(List<RecipeModel> object) {
                mRecepiesList.add(object);

                RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(mRecepiesList, getContext());
                mainRecyclerView.setAdapter(adapter);



            }

            @Override
            public void onFailureCallback(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

        });

        return view;
    }


}