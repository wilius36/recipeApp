package com.example.recipeapp.Views.Fragments.ActiveUserFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recipeapp.Models.IngredientsModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.Models.UserModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.RecipeViewModel;
import com.example.recipeapp.ViewModels.UserViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PreviewRecipeFragment extends Fragment {

    private static final String TAG = "PreviewRecipeFragment";

    private View view;
    private String recipeId;
    private TextView dishTitleTextView;
    private TextView mealTypeTextView;
    private TextView shortDescriptionTextView;
    private TextView factureDificultyTextView;
    private TextView dishRatingTextView;
    private TextView ingredientsTextView;
    private TextView factureDescriptionTextView;
    private TextView postDateTextView;
    private Spinner ratingSpinner;
    private Button recipeRatingButton;
    private ArrayList<String> favoriteList;

    private RecipeViewModel recipeViewModel;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_preview_recipe, container, false);

        recipeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);


        recipeId = getArguments().getString("recipeId");
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        favoriteList = new ArrayList<String>();

        viewsInitialization();



        loadRecipeData();

        recipeRatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecipeModel recipeModel = getRecipeModel();
                recipeViewModel.rateRecipe(recipeModel, new MyFirebaseCallBack<Boolean>() {
                    @Override
                    public void onSuccessCallback(Boolean object) {
                        Toast.makeText(getContext(), "Pavyko ivertinti", Toast.LENGTH_SHORT).show();
                        recipeRatingButton.setEnabled(false);
                        recipeRatingButton.setText("Recepta jau įvertinai");
                    }

                    @Override
                    public void onFailureCallback(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return view;
    }



    //Uzkraunamas receptas
    private void loadRecipeData() {
        recipeViewModel.previewRecipe(recipeId, new MyFirebaseCallBack<RecipeModel>() {
            @Override
            public void onSuccessCallback(RecipeModel object) {
                dishTitleTextView.setText(object.getTitle());
                mealTypeTextView.setText(object.getMealType());
                shortDescriptionTextView.setText(object.getShortDescription());
                factureDificultyTextView.setText(object.getFactureDificulty());
                factureDescriptionTextView.setText(object.getFactureDescription());

                int sumOfRating = object.getSumOfRating();
                if (sumOfRating != 0) {
                    double rating = sumOfRating / object.getTotalPeopleRatedCount();
                    int value = (int)Math.round(rating);
                    dishRatingTextView.setText("Įvertino " + object.getTotalPeopleRatedCount() +" .Reitingas " + value);
                } else {
                    dishRatingTextView.setText("Nera dar ivertinimo");
                }

                ArrayList<String> usersIdWhoRated = (ArrayList<String>) object.getUsersWhoRatedRecipeId();

                if (usersIdWhoRated != null) {
                    if (usersIdWhoRated.contains(currentUser.getUid())) {
                        recipeRatingButton.setEnabled(false);
                        recipeRatingButton.setText("Recepta jau įvertinai");
                    } else {
                        recipeRatingButton.setEnabled(true);
                        recipeRatingButton.setText("Įvertinti");
                    }
                }

                ArrayList<IngredientsModel> arrayList = (ArrayList<IngredientsModel>) object.getIngredients();
                ingredientsTextView.setText(arrayList.toString());

                Date date = new Date(object.getPostDate());
                DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                String d=f.format(date);

                postDateTextView.setText(d);
            }

            @Override
            public void onFailureCallback(String message) {
                Toast.makeText(getContext(), "Nepavyko užkrauti recepto", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Gaunama spinner pasirinkimo pozicija ir padidinama +1
    private RecipeModel getRecipeModel() {
        int count = ratingSpinner.getSelectedItemPosition();
        count++;
        RecipeModel recipeModel = new RecipeModel();
        recipeModel.setId(recipeId);
        recipeModel.setSumOfRating(count);
        return recipeModel;
    }

    //Vaizdu inicijavimas
    private void viewsInitialization() {
        dishTitleTextView = view.findViewById(R.id.dishTitleTextView);
        mealTypeTextView = view.findViewById(R.id.mealTypeTextView);
        shortDescriptionTextView = view.findViewById(R.id.shortDescriptionTextView);
        factureDificultyTextView = view.findViewById(R.id.factureDificultyTextView);
        dishRatingTextView = view.findViewById(R.id.dishRatingTextView);
        ingredientsTextView = view.findViewById(R.id.ingredientsTextView);
        factureDescriptionTextView = view.findViewById(R.id.factureDescriptionTextView);
        postDateTextView = view.findViewById(R.id.postDateTextView);
        ratingSpinner = view.findViewById(R.id.ratingSpinner);
        recipeRatingButton = view.findViewById(R.id.recipeRatingButton);


        ArrayAdapter<CharSequence> adapterDishDificultyType = ArrayAdapter.createFromResource(getContext(), R.array.dish_rating_spinner, android.R.layout.simple_spinner_item);
        adapterDishDificultyType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ratingSpinner.setAdapter(adapterDishDificultyType);
    }

}
