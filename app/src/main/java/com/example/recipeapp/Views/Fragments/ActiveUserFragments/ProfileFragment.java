package com.example.recipeapp.Views.Fragments.ActiveUserFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.recipeapp.Adapter.UserRecipesAdapter;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.Models.UserModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.RecipeViewModel;
import com.example.recipeapp.ViewModels.UserViewModel;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment {

    private static final String TAG = "ProfileFragment";

    private View view;
    private TextView usernamePersonTextView;
    private TextView emailPersonTextView;
    private RecyclerView myRecipesRecyclerView;
    private UserViewModel userViewModel;
    private RecipeViewModel recipeViewModel;

    private List<RecipeModel> mRecepie;
    private UserRecipesAdapter recepieAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_person, container, false);

        viewInitialization();

        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
        recipeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);

        myRecipesRecyclerView = view.findViewById(R.id.myRecipesRecyclerView);
        myRecipesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mRecepie = new ArrayList<>();

        updateViews();

        updateRecipeRecyclerView();

        return view;
    }

    //Uzkraunami vartotojo sukurti receptai
    private void updateRecipeRecyclerView () {

        recipeViewModel.getUserRecipes(new MyFirebaseCallBack<RecipeModel>() {
            @Override
            public void onSuccessCallback(RecipeModel object) {
                mRecepie.add(object);
                recepieAdapter = new UserRecipesAdapter(getContext(), mRecepie);
                myRecipesRecyclerView.setAdapter(recepieAdapter);
            }

            @Override
            public void onFailureCallback(String message) {

            }
        });

    }

    //Vaizdiniu atnaujinimas duomenimis is duombazes
    private void updateViews() {
        userViewModel.getUserData(new MyFirebaseCallBack<UserModel>(){

            @Override
            public void onSuccessCallback(UserModel object) {
                usernamePersonTextView.setText(object.getUsername());
                emailPersonTextView.setText(object.getEmail());
            }

            @Override
            public void onFailureCallback(String message) {

            }
        });
    }

    //Vaizdu inicijavimas
    private void viewInitialization() {
        usernamePersonTextView = view.findViewById(R.id.usernamePersonTextView);
        myRecipesRecyclerView = view.findViewById(R.id.myRecipesRecyclerView);
        emailPersonTextView = view.findViewById(R.id.emailPersonTextView);
    }
}
