package com.example.recipeapp.Views.Fragments.RecipeAddFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.ViewModels.RecipeViewModel;

public class FirstAddRecipeFragment extends Fragment {

    private View view;
    private Button firstSavingRecipeButton;
    private Spinner categorySpinnerAddDish;
    private Spinner factoryDescriptionSpinner;
    private TextView recipeTitleEditText;
    private TextView recipeDescriptionEditText;

    private RecipeViewModel recipeViewModel;
    private RecipeModel recipeModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_first_saving_recipe, container, false);

        recipeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);

        recipeModel = recipeViewModel.getRecipeData();

        viewsInitilization();

        spinnerInitialization();

        textInputType();

        firstSavingRecipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecipeDataFirstFragment();
            }
        });

        return view;
    }

    //Prideti duomenys i recipe modeli
    private void setRecipeDataFirstFragment() {

        if (categorySpinnerAddDish.getSelectedItem().toString().matches("Pasirinkite patiekalo tipą")
                || TextUtils.isEmpty(recipeTitleEditText.getText().toString())
                || TextUtils.isEmpty(recipeDescriptionEditText.getText().toString())
                || factoryDescriptionSpinner.getSelectedItem().toString().matches("Nurodykite paruošimo sunkumą")){
            Toast.makeText(getContext(), "Užpildykite visus laukelius reikalingus įkelti receptą", Toast.LENGTH_SHORT).show();
        } else {
            recipeModel.setMealType(categorySpinnerAddDish.getSelectedItem().toString());
            recipeModel.setTitle(recipeTitleEditText.getText().toString());
            recipeModel.setDescription(recipeDescriptionEditText.getText().toString());
            recipeModel.setFactureDificulty(factoryDescriptionSpinner.getSelectedItem().toString());

            openNextFragment();
        }
    }

    //Teksto ivedimo valdymas
    private void textInputType() {
        recipeTitleEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        recipeDescriptionEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    }

    //Spinerio inicijavimas
    private void spinnerInitialization() {
        ArrayAdapter<CharSequence> adapterDishType = ArrayAdapter.createFromResource(getContext(), R.array.dish_type_array, android.R.layout.simple_spinner_item);
        adapterDishType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinnerAddDish.setAdapter(adapterDishType);

        ArrayAdapter<CharSequence> adapterDishDificultyType = ArrayAdapter.createFromResource(getContext(), R.array.dish_dificulty_array, android.R.layout.simple_spinner_item);
        adapterDishDificultyType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        factoryDescriptionSpinner.setAdapter(adapterDishDificultyType);
    }

    //Vaizdu inicijavimas
    private void viewsInitilization() {
        firstSavingRecipeButton = view.findViewById(R.id.firstSavingRecipeButton);
        categorySpinnerAddDish = view.findViewById(R.id.categorySpinnerAddDish);
        factoryDescriptionSpinner = view.findViewById(R.id.factoryDescriptionSpinner);
        recipeTitleEditText = view.findViewById(R.id.recipeTitleEditText);
        recipeDescriptionEditText = view.findViewById(R.id.recipeDescriptionEditText);
    }

    //Atidaro sekanti fragmenta
    private void openNextFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, new SecondAddRecipeFragment(), "FirstAddRecipeFragment")
                .addToBackStack(null)
                .commit();
    }

}
