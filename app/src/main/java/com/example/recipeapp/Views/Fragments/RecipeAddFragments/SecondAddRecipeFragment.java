package com.example.recipeapp.Views.Fragments.RecipeAddFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.ViewModels.RecipeViewModel;

public class SecondAddRecipeFragment extends Fragment {

    private View view;
    private Button secondSavingRecipeButton;
    private EditText productionDescriptionEditText;

    private RecipeViewModel recipeViewModel;
    private RecipeModel recipeModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_second_saving_recipe, container, false);

        recipeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);

        recipeModel = recipeViewModel.getRecipeData();

        secondSavingRecipeButton = view.findViewById(R.id.secondSavingRecipeButton);
        productionDescriptionEditText = view.findViewById(R.id.productionDescriptionEditText);

        productionDescriptionEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        secondSavingRecipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecipeDataSecondFragment();
            }
        });

        return view;
    }

    //Prideti duomenys i recipe modeli
    private void setRecipeDataSecondFragment() {
        if (TextUtils.isEmpty(productionDescriptionEditText.getText().toString())){
            Toast.makeText(getContext(), "Aprašykite recepto gaminimo eigą", Toast.LENGTH_SHORT).show();
        } else {
            recipeModel.setFactureDescription(productionDescriptionEditText.getText().toString());
            openNextFragment();
        }
    }

    //Atidaro kita fragmenta
    private void openNextFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, new ThirdAddRecipeFragment(), "SecondAddRecipeFragment")
                .addToBackStack("FirstAddRecipeFragment")
                .commit();
    }

}
