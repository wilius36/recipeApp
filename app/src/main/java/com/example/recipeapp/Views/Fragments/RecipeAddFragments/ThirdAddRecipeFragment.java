package com.example.recipeapp.Views.Fragments.RecipeAddFragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.recipeapp.Models.IngredientsModel;
import com.example.recipeapp.Models.RecipeModel;
import com.example.recipeapp.R;
import com.example.recipeapp.Utils.MyFirebaseCallBack;
import com.example.recipeapp.ViewModels.RecipeViewModel;

import java.util.ArrayList;

public class ThirdAddRecipeFragment extends Fragment {

    private View view;
    private EditText ingredientNameEditText;
    private EditText ingredientAmountEditText;
    private ImageButton addIngredientButton;
    private Button addRecipeButton;
    private ListView ingredientsListView;
    private ArrayList<IngredientsModel> ingredientsArrayList;
    private ArrayList<String> ingredientsDisplayArrayList;
    private ArrayAdapter<String> arrayDisplayAdapter;

    private RecipeViewModel recipeViewModel;
    private RecipeModel recipeModel;
    private IngredientsModel ingredientsModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_third_saving_recipe, container, false);

        recipeViewModel = ViewModelProviders.of(getActivity()).get(RecipeViewModel.class);

        recipeModel = recipeViewModel.getRecipeData();

        viewsInitilization();

        textInputType();

        addIngredientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIngredients();
            }
        });

        addRecipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecipeDataThirdFragment();
            }
        });

        return view;
    }

    //Duomenu uzsetinimas recipe klaseje
    private void setRecipeDataThirdFragment() {
        if (ingredientsArrayList.isEmpty()) {
            Toast.makeText(getContext(), "Įveskite reikalingus ingredientus receptui", Toast.LENGTH_SHORT).show();
        } else {
            recipeModel.setIngredients(ingredientsArrayList);
            long time = System.currentTimeMillis();
            recipeModel.setPostDate(time);

            recipeViewModel.saveRecipe(new MyFirebaseCallBack<Boolean>() {
                @Override
                public void onSuccessCallback(Boolean object) {
                    Toast.makeText(getContext(), "Receptas įkeltas sėkmingai", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailureCallback(String message) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //Teksto ivedimo valdymas
    private void textInputType() {
        ingredientNameEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        ingredientAmountEditText.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    //Ingredientu pridejimas
    private void addIngredients() {
        String ingredientName = ingredientNameEditText.getText().toString();
        String ingredientAmount = ingredientAmountEditText.getText().toString();

        ingredientsModel = new IngredientsModel();

        ingredientsModel.setIngredientName(ingredientName);
        ingredientsModel.setAmount(ingredientAmount);

        ingredientsArrayList.add(ingredientsModel);
        ingredientsDisplayArrayList.add(ingredientName + " " + ingredientAmount);
        arrayDisplayAdapter.notifyDataSetChanged();

        ingredientAmountEditText.setText("");
        ingredientNameEditText.setText("");
    }

    //Vaizdu inicijavimas
    private void viewsInitilization() {
        ingredientNameEditText = view.findViewById(R.id.ingredientNameEditText);
        ingredientAmountEditText = view.findViewById(R.id.ingredientAmountEditText);
        addIngredientButton = view.findViewById(R.id.addIngredientButton);
        ingredientsListView = view.findViewById(R.id.ingredientsListView);
        addRecipeButton = view.findViewById(R.id.addRecipeButton);

        ingredientsArrayList = new ArrayList<IngredientsModel>(  );
        ingredientsDisplayArrayList = new ArrayList<String>(  );

        arrayDisplayAdapter = new ArrayAdapter<String>( getContext(), android.R.layout.simple_list_item_1, ingredientsDisplayArrayList );
        ingredientsListView.setAdapter( arrayDisplayAdapter );
    }

}
